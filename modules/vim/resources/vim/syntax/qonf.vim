" Vim syntax file
" Language: QD configuration file

if exists("b:current_syntax")
  finish
endif

syn match   qdComments          '^\s*#.*'
syn match   qdBlockOpen '#{{'
syn match   qdBlockClose '#}}'
syn region  qdCommentsBlock start='^\s*#{' end='^\s*#}'

syn region  qdOptionBlock start='^\s*#{{\n\s*#[!?*+$%]\s' end='^\s*#}}' keepend contains=qdBlockOpen,qdBlockClose
syn region  qdActionBlock start='^\s*#{{\n\s*[a-z][a-z0-9-.]*\:'  end='^\s*#}}' keepend contains=qdActionType,qdBlockOpen,qdBlockClose

syn region  qdLoop              start='^\s*#\[' end='$' oneline contains=qdLoopLineCont
syn match   qdLoopLineCont      '\\$' contained

syn region  qdAction            start='^\s*[a-z][a-z0-9-.]*\:' end='$' oneline contains=qdActionLineCont,qdActionType
syn match   qdActionLineCont    '\\$' contained
syn match   qdActionType        '^\s*[a-z][a-z0-9-.]*\:' contained

syn region  qdOption            start='^\s*#[!?*+$%]\s' end='$' oneline contains=qdOptionLineCont
syn region  qdInclude           start='^\s*#<[!?]\?\s' end='$'
syn region  qdAlias             start='^\s*#[=]\s' end='$' oneline contains=qdAliasLineCont
syn match   qdOptionLineCont    '\\$' contained
syn match   qdAliasLineCont     '\\$' contained

hi link qdActionType        Identifier
hi link qdAction            String
hi link qdActionLineCont    String
hi link qdActionBlock       String
hi link qdOption            Special
hi link qdOptionLineCont    Special
hi link qdOptionBlock       Special
hi link qdInclude           Special
hi link qdAlias             Function
hi link qdAliasLineCont     Function
hi link qdComments          Comment
hi link qdCommentsBlock     Comment
hi link qdLoop              Comment
hi link qdLoopLineCont      Comment
hi link qdBlockOpen         Comment
hi link qdBlockClose        Comment

let b:current_syntax = "qonf"
