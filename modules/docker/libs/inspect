#!/bin/bash


qdl_docker_inspect_container_status ()
{
    local args container status

    qd_func "Get the container status." "$@"
    {
        qd_func_arg "<container>" "The container name."
        qd_func_arg "[status]" "The status to check."
    }

    local st=$(docker container inspect $container --format "{{.State.Status}}" 2> /dev/null)

    if [ -n "$status" ]; then
        [ "$status" = "$st" ]
    else
        echo -n "$st"
    fi
}


qdl_docker_inspect_container_ports ()
{
    local args container

    qd_func "Get ports used by a container." "$@"
    {
        qd_func_arg "<container>" "The container name."
    }

    local ports=$(docker inspect --format '{{range $p, $conf := .NetworkSettings.Ports}}{{(index $conf 0).HostPort}}:{{$p}} {{end}}' $container)
    local mapping

    for mapping in $ports
    do
        echo ${mapping%/*}
    done
}


qdl_docker_inspect_container_tag ()
{
    local args container

    qd_func "Get image tag of a container." "$@"
    {
        qd_func_arg "<container>" "The container name."
    }

    local image=$(docker container inspect "$container" --format "{{.Image}}")
    local repo_tags=$(docker image inspect "$image" --format "{{.RepoTags}}")
    local tag

    repo_tags=(${repo_tags:1:-1})

    for tag in ${repo_tags[@]}
    do
        if [ "${tag%:latest}" = "$tag" ]; then
            echo -n "$tag"
            return
        fi
    done
}

qdl_docker_inspect_container_aliases ()
{
    local args container

    qd_func "Get container network aliases." "$@"
    {
        qd_func_arg "<container>" "The container name."
    }

    local aliases=($(docker container inspect $container --format '{{range $k, $v := .NetworkSettings.Networks}}{{join .Aliases " "}} {{end}}'))

    echo "${aliases[@]}"
}
