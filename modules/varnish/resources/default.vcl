vcl 4.1;

{{# vmods}}
import {{.}};
{{/}}

{{# backends}}
backend {{%.$.name}}
{
    .host = "{{%.$.host}}";
    .port = "{{%.$.port}}";
}
{{/}}

{{#? websocket}}
sub vcl_recv
{
    if (req.http.upgrade ~ "(?i)websocket")
    {
        return (pipe);
    }
}


sub vcl_pipe
{
    if (req.http.upgrade)
    {
        set bereq.http.upgrade = req.http.upgrade;
        set bereq.http.connection = req.http.connection;
    }
}
{{/}}
