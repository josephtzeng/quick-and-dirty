if [ -d ~/.profile.d ]; then
    for i in $(\ls ~/.profile.d/* 2> /dev/null) ; do
        . $i
    done
fi

if [ -n "$BASH_VERSION" ] && [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi
