if [ "$LC_QD_NORC" = "true" ]; then
    return
fi

if [ -f /etc/environment ]; then
    . /etc/environment
fi

QD_SCRIPT="${QD_SCRIPT:-$(which qd)}"

if [ ! -f "$QD_SCRIPT" ]; then
    export PATH=~/.qd/bin:$PATH

    QD_SCRIPT="$(which qd)"
fi

if [ -f "$QD_SCRIPT" ]; then
    . "$QD_SCRIPT"
fi

if [ -d ~/.bashrc.d ]; then
    for i in $(\ls ~/.bashrc.d/* 2> /dev/null) ; do
        . $i
    done
fi

if [ -d ~/.bash_completion.d ]; then
    for i in $(\ls ~/.bash_completion.d/* 2> /dev/null) ; do
        . $i
    done
fi
