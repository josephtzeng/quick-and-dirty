#!/bin/bash

describe_command ()
{
    echo "Clone a repo."
}

describe_command_options ()
{
    local project

    if [ -n "$QDC_REPO" ]; then
        project=${QDC_REPO%.git}
        project=${project##*[[:punct:]]}
    fi

    qd_describe_command_option "dest" "string" "The destination dir." false "/tmp"
    qd_describe_command_option "repo" "string" "The project name." true
    qd_describe_command_option "project" "string" "The project name." true "$project"
    qd_describe_command_option "append.revision" "bool" "Append the revision to the destination dir." false false
    qd_describe_command_option "link.latest" "bool" "Create a symlink to the latest clone." false false
    qd_describe_command_option "branch" "string" "The branch to be checked out after clone." false "master"
}

run_command ()
{
    cd $QDC_DEST

    local project_dir=$QDC_PROJECT

    if $QDC_APPEND_REVISION; then
        local revision=$(git ls-remote $QDC_REPO $QDC_BRANCH -q | cut -f 1)

        [ -n "$revision" ] || qd_error "Unable to get the revision number."

        revision=${revision:0:7}

        project_dir="$QDC_PROJECT-$revision"
    fi

    if [ ! -d "$project_dir/.git" ]; then
        git clone -b $QDC_BRANCH "$QDC_REPO" "$project_dir"
    else
        echo "Repository $project_dir exists."
    fi

    if $QDC_LINK_LATEST; then
        qd_link "$project_dir" "$QDC_PROJECT-latest"
    fi
}
